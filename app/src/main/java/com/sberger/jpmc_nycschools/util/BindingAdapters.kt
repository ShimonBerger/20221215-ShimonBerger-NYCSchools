package com.sberger.jpmc_nycschools.util

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.sberger.jpmc_nycschools.R
import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.network.model.School

// calculates the percentage of SAT test-takers
@BindingAdapter("school", "satData")
fun getSatPercentage(textView: TextView, school: School?, satData: SatData?) {
    if (school == null || satData == null) {
        textView.text = ""
        return
    }

    try {
        val percentage = (satData.testTakerCount.toDouble() / school.totalStudents.toDouble() * 100)
        val roundedPercentage = String.format("%.2f", percentage) // round to 2 decimal places
        textView.text = textView.context.getString(R.string.percentage, roundedPercentage)
    } catch (e: Exception) {
        textView.text = textView.context.getString(R.string.na)
    }
}