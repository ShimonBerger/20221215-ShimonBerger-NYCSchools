package com.sberger.jpmc_nycschools.util

import android.text.Spannable
import android.text.TextPaint
import android.text.style.URLSpan
import android.view.View

// various UI utilities
class PresentationUtil {

    companion object {

        @JvmStatic
        fun stripUnderlines(text: CharSequence) {
            val s = text as Spannable
            val spans = s.getSpans(0, s.length, URLSpan::class.java)
            for (span in spans) {
                var currentSpan = span
                val start = s.getSpanStart(currentSpan)
                val end = s.getSpanEnd(currentSpan)
                s.removeSpan(currentSpan)
                currentSpan = URLSpanNoUnderline(currentSpan.url)
                s.setSpan(currentSpan, start, end, 0)
            }
        }

        @JvmStatic
        fun getVisibilityByString(string: String?): Int = when (string?.trim()) {
            null, "" -> View.GONE
            else -> View.VISIBLE
        }

    }

    private class URLSpanNoUnderline(url: String?) : URLSpan(url) {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
}