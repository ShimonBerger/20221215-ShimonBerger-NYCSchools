package com.sberger.jpmc_nycschools.util

import android.content.Context
import android.text.Spannable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

// special TextView to remove auto-generated underlines form autoLink
class NoUnderlineTextView(context: Context, attrs: AttributeSet?) : AppCompatTextView(context, attrs) {
    override fun setText(text: CharSequence?, type: BufferType?) {
        super.setText(text, type)
        if (getText() is Spannable) PresentationUtil.stripUnderlines(getText())
    }
}