package com.sberger.jpmc_nycschools.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.sberger.jpmc_nycschools.R
import com.sberger.jpmc_nycschools.data.network.model.School
import com.sberger.jpmc_nycschools.databinding.ViewSchoolListItemBinding

class SchoolListAdapter(private var schools: List<School>, private var onItemClicked: (School) -> Unit) : RecyclerView.Adapter<SchoolListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewSchoolListItemBinding>(inflater, R.layout.view_school_list_item, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(schools[position], onItemClicked)
    }

    fun setData(schools: List<School>) {
        this.schools = schools
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ViewSchoolListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School, onItemClicked: (School) -> Unit) {
            binding.name = school.name

            binding.root.setOnClickListener {
                onItemClicked(school)
            }

            // forces the binding to execute immediately, which allows the RecyclerView to make the correct view size measurements
            binding.executePendingBindings()
        }

    }
}