package com.sberger.jpmc_nycschools.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sberger.jpmc_nycschools.data.network.model.School
import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(private val repo: NycSchoolsRepository) : ViewModel() {

    // internal (mutable) data from data layer
    private val _schools = MutableLiveData<List<School>>()

    // exposed LiveData for UI layer (immutable)
    val schools: LiveData<List<School>> = _schools
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(true)

    // get school list when initialized
    init {
        getSchoolList()
    }

    fun getSchoolList() {
        isLoading.value = true

        viewModelScope.launch {
            try {
                val result = repo.getSchoolList()

                Log.d(this@SchoolListViewModel.javaClass.simpleName, "getSchoolList success. ${result.size} schools retrieved}")
                _schools.value = result
                isLoading.value = false
            } catch (e: Exception) {
                Log.e(this@SchoolListViewModel.javaClass.simpleName, "getSchoolList error: ${e.message}")
                isLoading.value = false
            }
        }
    }

}