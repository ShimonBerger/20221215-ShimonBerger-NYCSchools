package com.sberger.jpmc_nycschools.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.facebook.shimmer.ShimmerFrameLayout
import com.sberger.jpmc_nycschools.R
import com.sberger.jpmc_nycschools.data.network.model.School
import com.sberger.jpmc_nycschools.databinding.FragmentSchoolListBinding
import com.sberger.jpmc_nycschools.ui.details.SchoolDetailsFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListFragment : Fragment() {
    private lateinit var binding: FragmentSchoolListBinding
    private lateinit var viewModel: SchoolListViewModel
    private lateinit var shimmerFrame: ShimmerFrameLayout
    private lateinit var adapter: SchoolListAdapter

    companion object {
        fun newInstance() = SchoolListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SchoolListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSchoolListBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner

        shimmerFrame = binding.shimmerFrame

        adapter = SchoolListAdapter(mutableListOf(), ::onSchoolClicked)
        binding.schoolListRecyclerView.adapter = adapter
        binding.schoolListRecyclerView.setEmptyView(binding.errorText)

        // set up swipe to refresh
        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = false
            viewModel.getSchoolList()
        }

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.schools.observe(viewLifecycleOwner) {
            adapter.setData(it)
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                // hide everything besides shimmer frame when loading
                binding.schoolListRecyclerView.visibility = View.INVISIBLE
                binding.errorText.visibility = View.INVISIBLE
                shimmerFrame.visibility = View.VISIBLE
                shimmerFrame.startShimmer()
            } else {
                // stop and hide shimmer, then show error state if there is no data, otherwise show the data we have
                shimmerFrame.stopShimmer()
                shimmerFrame.visibility = View.GONE
                binding.schoolListRecyclerView.checkIfEmpty()
            }
        }
    }

    // click listener when a school is selected (passed into adapter)
    private fun onSchoolClicked(school: School) {
        try {
            parentFragmentManager.beginTransaction()
//                    .setCustomAnimations(
//                        android.R.anim.slide_out_right
//                    )
                .replace(R.id.container, SchoolDetailsFragment.newInstance(school))
                .addToBackStack(null)
                .commit()
        } catch (e: Exception) {
            Log.e(this.javaClass.simpleName, e.message.orEmpty())
        }
    }
}