package com.sberger.jpmc_nycschools.ui.details

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.sberger.jpmc_nycschools.data.network.model.School
import com.sberger.jpmc_nycschools.databinding.FragmentSchoolDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {
    private lateinit var school: School
    private lateinit var binding: FragmentSchoolDetailsBinding
    private lateinit var viewModel: SchoolDetailsViewModel
//    private lateinit var shimmerFrame: ShimmerFrameLayout

    companion object {
        private const val ARG_SCHOOL = "arg_school"

        fun newInstance(school: School) = SchoolDetailsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_SCHOOL, school)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arg: School? = requireArguments().getParcelable(ARG_SCHOOL)
        if (arg == null) {
            throw IllegalStateException("School data is missing")
        } else school = arg

        viewModel = ViewModelProvider(this).get(SchoolDetailsViewModel::class.java)
        viewModel.getSatData(school.dbn)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSchoolDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.school = school

//        shimmerFrame = binding.shimmerFrame

        // remove extra highlighting. xml attribute doesn't work for this
        binding.locationText.highlightColor = Color.TRANSPARENT
        binding.phoneText.highlightColor = Color.TRANSPARENT
        binding.emailText.highlightColor = Color.TRANSPARENT
        binding.websiteText.highlightColor = Color.TRANSPARENT

        // go back to list when clicking back arrow
        binding.toolbar.setNavigationOnClickListener {
            parentFragmentManager.popBackStack()
        }

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.satData.observe(viewLifecycleOwner) {
            binding.satData = it
        }

//        viewModel.isLoading.observe(viewLifecycleOwner) {
//            if (it) shimmerFrame.startShimmer()
//            else {
//                shimmerFrame.stopShimmer()
//                shimmerFrame.visibility = View.GONE
//            }
//        }
    }

}