package com.sberger.jpmc_nycschools.ui.details

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(private val repo: NycSchoolsRepository) : ViewModel() {

    // internal (mutable) data from data layer
    private val _satData = MutableLiveData<SatData>()

    // exposed (immutable) LiveData for UI layer
    val satData: LiveData<SatData> = _satData
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(true)

    fun getSatData(dbn: String) {
        isLoading.value = true

        viewModelScope.launch {
            try {
                val result = repo.getSatData(dbn)

                Log.d(this@SchoolDetailsViewModel.javaClass.simpleName, "getSatData success for dbn: ${result.dbn} and name: ${result.name}")
                _satData.value = result
                isLoading.value = false
            } catch (e: Exception) {
                Log.e(this@SchoolDetailsViewModel.javaClass.simpleName, "getSatData error: ${e.message}")
                _satData.value = SatData()
                isLoading.value = false
            }
        }
    }

}