package com.sberger.jpmc_nycschools.data.network.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

// data class for School data, including only relevant data for this application
// mapped using Moshi from json response
@Parcelize
data class School(
    @Json(name = "dbn")
    val dbn: String,

    @Json(name = "school_name")
    val name: String,

    @Json(name = "overview_paragraph")
    val overview: String,

    @Json(name = "phone_number")
    val phoneNumber: String,

    @Json(name = "school_email")
    val email: String?,

    @Json(name = "website")
    val website: String,

    @Json(name = "total_students")
    val totalStudents: String,

    @Json(name = "primary_address_line_1")
    val address: String,

    @Json(name = "city")
    val city: String,

    @Json(name = "state_code")
    val state: String,

    @Json(name = "zip")
    val zip: String,

    @Json(name = "latitude")
    val latitude: String?,

    @Json(name = "longitude")
    val longitude: String?
) : Parcelable {
    fun getFullAddress(): String {
        return "$address\n$city, $state $zip"
    }
}