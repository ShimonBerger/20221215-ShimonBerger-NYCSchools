package com.sberger.jpmc_nycschools.data.repository

import com.sberger.jpmc_nycschools.data.network.NycSchoolsApi
import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.network.model.School
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NycSchoolsRepositoryImpl : NycSchoolsRepository {

    // implement repo functions

    override suspend fun getSchoolList(): MutableList<School> {
        val result: MutableList<School>

        // run network call on IO thread
        withContext(Dispatchers.IO) {
            // call network to get list of schools
            result = NycSchoolsApi.retrofitService.getSchoolList().toMutableList()
            result.sortBy { it.name } // sort schools alphabetically
        }
        return result
    }

    override suspend fun getSatData(dbn: String): SatData {
        val result: SatData

        // run network call on IO thread
        withContext(Dispatchers.IO) {
            // call network to get SAT data for the selected school. response is a list, so need to pull out the first object
            result = NycSchoolsApi.retrofitService.getSatData(dbn).first()
        }
        return cleanData(result)
    }

    private fun cleanData(data: SatData): SatData {
        // standardize invalid data
        return data.copy(
            testTakerCount = if (data.testTakerCount == "s") "N/A" else data.testTakerCount,
            avgReading = if (data.avgReading == "s") "N/A" else data.avgReading,
            avgMath = if (data.avgMath == "s") "N/A" else data.avgMath,
            avgWriting = if (data.avgWriting == "s") "N/A" else data.avgWriting
        )
    }

}
