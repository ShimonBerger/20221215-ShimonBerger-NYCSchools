package com.sberger.jpmc_nycschools.data.network

import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.network.model.School
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface INycSchoolsApi {
    // endpoint for school list
    @GET("s3k6-pzi2")
    suspend fun getSchoolList(): List<School>

    // endpoint for sat data. dbn is passed as a query param to only get the data for that school
    @GET("f9bf-2cp4")
    suspend fun getSatData(@Query("dbn") dbn:String): List<SatData>
}

object NycSchoolsApi {
    val retrofitService: INycSchoolsApi by lazy {
        retrofit.create(INycSchoolsApi::class.java)
    }
}