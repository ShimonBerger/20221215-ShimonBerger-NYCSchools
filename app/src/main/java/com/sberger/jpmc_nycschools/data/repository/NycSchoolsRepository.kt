package com.sberger.jpmc_nycschools.data.repository

import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.network.model.School

interface NycSchoolsRepository {
    suspend fun getSchoolList(): MutableList<School>
    suspend fun getSatData(dbn: String): SatData
}