package com.sberger.jpmc_nycschools.data.network.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

// data class for SAT data
// mapped using Moshi from json response
@Parcelize
data class SatData(
    @Json(name = "dbn")
    val dbn: String = "N/A",

    @Json(name = "school_name")
    val name: String = "N/A",

    @Json(name = "num_of_sat_test_takers")
    val testTakerCount: String = "N/A",

    @Json(name = "sat_critical_reading_avg_score")
    val avgReading: String = "N/A",

    @Json(name = "sat_math_avg_score")
    val avgMath: String = "N/A",

    @Json(name = "sat_writing_avg_score")
    val avgWriting: String = "N/A"
) : Parcelable {
    fun getCombinedAverage(): String {
        return try {
            (avgReading.toInt() + avgMath.toInt() + avgWriting.toInt()).toString()
        } catch (e: Exception) {
            "N/A"
        }
    }
}