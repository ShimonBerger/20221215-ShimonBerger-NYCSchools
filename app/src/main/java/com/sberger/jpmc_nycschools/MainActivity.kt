package com.sberger.jpmc_nycschools

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sberger.jpmc_nycschools.ui.main.SchoolListFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SchoolListFragment.newInstance())
                .commitNow()
        }
    }
}
/*
things to add with time (in no order):
    1. pagination
    2. favoriting
    3. search
    4. filter/sort
    5. map on details screen
    6. caching
    7. transition animations
    8. more unit tests
 */