package com.sberger.jpmc_nycschools.di

import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepository
import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideNycSchoolsRepository(): NycSchoolsRepository = NycSchoolsRepositoryImpl()

}