package com.sberger.jpmc_nycschools.network.model

import com.sberger.jpmc_nycschools.data.network.model.SatData
import org.junit.Assert.assertEquals
import org.junit.Test

internal class SatDataTest {

    @Test
    internal fun `when scores are valid then combined score should return proper result`() {
        val data = SatData(avgReading = "100", avgMath = "200", avgWriting = "300")
        assertEquals("600", data.getCombinedAverage())
    }

    @Test
    internal fun `when scores are not valid then combined score should return proper result`() {
        val data = SatData(avgReading = "100", avgMath = "math", avgWriting = "300")
        assertEquals("N/A", data.getCombinedAverage())
    }
}