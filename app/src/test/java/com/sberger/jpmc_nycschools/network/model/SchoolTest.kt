package com.sberger.jpmc_nycschools.network.model

import com.sberger.jpmc_nycschools.data.network.model.School
import org.junit.Assert.assertEquals
import org.junit.Test

class SchoolTest {
    @Test
    internal fun `when scores are valid then combined score should return proper result`() {
        val school = School("", "", "", "", "", "", "",
            "123 Main St",
            "New York",
            "NY",
            "11111",
            "", "")

        assertEquals("123 Main St\nNew York, NY 11111", school.getFullAddress())
    }
}