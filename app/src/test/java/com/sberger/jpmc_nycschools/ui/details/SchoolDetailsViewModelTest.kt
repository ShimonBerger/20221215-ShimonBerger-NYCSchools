package com.sberger.jpmc_nycschools.ui.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sberger.jpmc_nycschools.MainCoroutineDispatcherRule
import com.sberger.jpmc_nycschools.data.network.model.SatData
import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolDetailsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutinesRule = MainCoroutineDispatcherRule()


    @Test
    fun `getSatData should set proper data`() = runTest {
        val data = SatData("123", "name", "10", "100", "200", "300")

        val mockRepo = mock(NycSchoolsRepository::class.java)
        given(mockRepo.getSatData(anyString())).willReturn(data)
        val schoolDetailsViewModel = SchoolDetailsViewModel(mockRepo)

        schoolDetailsViewModel.getSatData("123")
        advanceUntilIdle()

        assertEquals(data, schoolDetailsViewModel.satData.value)
    }

}