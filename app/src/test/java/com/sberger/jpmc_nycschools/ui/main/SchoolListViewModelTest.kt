package com.sberger.jpmc_nycschools.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sberger.jpmc_nycschools.MainCoroutineDispatcherRule
import com.sberger.jpmc_nycschools.data.network.model.School
import com.sberger.jpmc_nycschools.data.repository.NycSchoolsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mockito

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolListViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutinesRule = MainCoroutineDispatcherRule()


    @Test
    fun `getSchoolList should set proper data`() = runTest {
        val data = mutableListOf(
            School(
                "123",
                "name",
                "overview",
                "6145551234",
                "school@school.edu",
                "school.edu",
                "100",
                "123 Main St",
                "New York",
                "NY",
                "12345",
                "",
                ""
            )
        )

        val mockRepo = Mockito.mock(NycSchoolsRepository::class.java)
        given(mockRepo.getSchoolList()).willReturn(data)
        val schoolListViewModel = SchoolListViewModel(mockRepo)

        schoolListViewModel.getSchoolList()
        advanceUntilIdle()

        assertEquals(data, schoolListViewModel.schools.value)
    }

}